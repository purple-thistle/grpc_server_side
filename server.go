package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"

	log "github.com/sirupsen/logrus"
	"github.com/soheilhy/cmux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "gitlab.com/purple-thistle/grpc_server_side_api"
)

const (
	port = ":50051"
)

func main() {

	log.SetLevel(log.TraceLevel)
	ctx := context.Background()
	ctx, ctxCancel := context.WithCancel(ctx)
	defer ctxCancel()

	listener := listenOnAddress(port)

	m := cmux.New(listener)
	grpcListener := m.Match(cmux.HTTP2())
	httpListener := m.Match(cmux.Any())

	grpcServer := startGRPCServer(grpcListener)
	cancel := startHTTPServer(httpListener, registerHTTPServer)
	defer cancel()
	go func() {
		err := m.Serve()
		if err != nil {
			log.Fatalln("Cannot start server")
		}
	}()

	// Right way to stop the server using a SHUTDOWN HOOK
	// Create a channel to receive OSType signals
	c := make(chan os.Signal, 1)

	// Relay os.Interrupt to our channel (os.Interrupt = CTRL+C)
	// Ignore other incoming signals
	signal.Notify(c, os.Interrupt)

	// Block main routine until a signal is received
	// As long as user doesn't press CTRL+C a message is not passed and our main routine keeps running
	<-c

	fmt.Println("\nStopping the server")
	ctxCancel()
	grpcServer.Stop()
	fmt.Println("Done.")

}

func listenOnAddress(address string) net.Listener {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Unable to listen on port %v : %v", address, err)
	}
	return listener
}

func startGRPCServer(listener net.Listener) *grpc.Server {
	s := grpc.NewServer()

	pb.RegisterCreditCardServer(s, &CreditCardServer{})
	pb.RegisterMorseCodeServer(s, &MorseCodeServer{})
	pb.RegisterHealthServer(s, &HealthServer{})

	reflection.Register(s)

	go func() {
		if err := s.Serve(listener); err != nil {
			log.Fatalf("Failed to start server : %v", err)
		}
	}()

	log.Info("GRPC server successfully started")
	return s
}

func startHTTPServer(listener net.Listener, register func(context.Context, *runtime.ServeMux, []grpc.DialOption)) context.CancelFunc {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	// Register gRPC server endpoint
	gatewayMux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	register(ctx, gatewayMux, opts)

	go func() {
		err := http.Serve(listener, gatewayMux)
		if err != nil {
			log.Fatalln("Cannot start http server")
		}
	}()
	log.Info("HTTP server successfully started")
	return cancel
}

func registerHTTPServer(ctx context.Context, mux *runtime.ServeMux, opts []grpc.DialOption) {
	err := pb.RegisterHealthHandlerFromEndpoint(ctx, mux, port, opts)
	killServerIfErr(err)
	err = pb.RegisterCreditCardHandlerFromEndpoint(ctx, mux, port, opts)
	killServerIfErr(err)
	err = pb.RegisterMorseCodeHandlerFromEndpoint(ctx, mux, port, opts)
	killServerIfErr(err)
}

func killServerIfErr(err error) {
	if err != nil {
		log.Fatalf("Unable to start server, %v", err)
	}
}
