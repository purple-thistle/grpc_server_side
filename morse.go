package main

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strings"

	pb "gitlab.com/purple-thistle/grpc_server_side_api"
)

const (
	separatorSpace = " "
	separatorSlash = "/"
)

var morseFormat = regexp.MustCompile("^[ /.-]*$")

var morsemap = map[string]string{
	".-":     "A",
	"-...":   "B",
	"-.-.":   "C",
	"-..":    "D",
	".":      "E",
	"..-.":   "F",
	"--.":    "G",
	"....":   "H",
	"..":     "I",
	".---":   "J",
	"-.-":    "K",
	".-..":   "L",
	"--":     "M",
	"-.":     "N",
	"---":    "O",
	".--.":   "P",
	"--.-":   "Q",
	".-.":    "R",
	"...":    "S",
	"-":      "T",
	"..-":    "U",
	"...-":   "V",
	".--":    "W",
	"-..-":   "X",
	"-.--":   "Y",
	"--..":   "Z",
	".----":  "1",
	"..---":  "2",
	"...--":  "3",
	"....-":  "4",
	".....":  "5",
	"-....":  "6",
	"--...":  "7",
	"---..":  "8",
	"----.":  "9",
	"-----":  "0",
	".-.-.-": ".",
	"--..--": ",",
	"..--..": "?",
	"-.-.--": "!",
	"-....-": "-",
	"-..-.":  "/",
	".--.-.": "@",
	"-.--.":  "(",
	"-.--.-": ")",
}

type MorseCodeServer struct {
	pb.UnsafeMorseCodeServer
}

func (m *MorseCodeServer) TranslateFromMorse(ctx context.Context, input *pb.MorseCodeRequest) (*pb.MorseCodeResponse, error) {
	fmt.Println("Decoding morse code")
	decodedMessage, err := decode(input.MessageInMorse)
	if err != nil {
		return nil, err
	}
	return &pb.MorseCodeResponse{MessageInEnglish: decodedMessage}, nil
}

func decode(message string) (string, error) {
	if !morseFormat.MatchString(message) {
		return "", errors.New("can not decode, message is in invalid format")
	}

	words := strings.Split(message, separatorSlash)
	if len(words) == 1 && message == "" {
		return "", nil
	}

	messageInProgress := make([]string, len(words))

	for i, word := range words {
		decodedWord, err := decodeWord(word)
		if err != nil {
			return "", err
		}
		// messageInProgress = append(messageInProgress, decodedWord)
		messageInProgress[i] = decodedWord
	}
	decodedMessage := strings.Join(messageInProgress, " ")

	decodedMessage = strings.TrimSpace(decodedMessage)
	return decodedMessage, nil
}

func decodeLetter(letter string) (string, error) {
	// what is the desired behavior for empty string?
	// if letter == "" {
	// 	return "", nil
	// }

	decodedLetter, ok := morsemap[letter]
	if !ok {
		return "", errors.New("morse letter not found")
	}
	return decodedLetter, nil
}

func decodeWord(word string) (string, error) {
	decodedWord := make([]string, len(word))
	letters := strings.Split(word, separatorSpace)

	for i, letter := range letters {
		decodedLetter, err := decodeLetter(letter)
		if err != nil {
			return "", err
		}
		// decodedWord = append(decodedWord, decodedLetter)
		decodedWord[i] = decodedLetter
	}
	return strings.Join(decodedWord, ""), nil
}
