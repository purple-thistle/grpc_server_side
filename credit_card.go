package main

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	pb "gitlab.com/purple-thistle/grpc_server_api"
)

var onlyDigits = regexp.MustCompile("^[0-9]*$")

type CreditCardServer struct {
	pb.UnsafeCreditCardServer
}

func (c *CreditCardServer) ValidateCreditCard(ctx context.Context, input *pb.CreditCardRequest) (*pb.CreditCardResponse, error) {
	fmt.Println("Checking if the credit card number is valid")

	err := validate(input.CreditCardNumber)
	if err != nil {
		return nil, err
	}
	return &pb.CreditCardResponse{Valid: true}, nil
}

func validate(ccNumber string) error {
	ccNumber = strings.ReplaceAll(ccNumber, " ", "")
	ccNumber = strings.ReplaceAll(ccNumber, "-", "")

	if len(ccNumber) < 2 {
		return errors.New("credit card number should have at least 2 digits")
	}

	if !onlyDigits.MatchString(ccNumber) {
		return errors.New("credit card number should consist of digits only")
	}

	var sum int

	reverseCCNumber := reverseString(ccNumber)

	for i, char := range reverseCCNumber {
		digit, _ := strconv.Atoi(string(char))
		pos := i + 1
		if isEven(pos) {
			digit *= 2
			if digit > 9 {
				digit -= 9
			}
		}
		sum += digit
	}

	if !isDivisible(sum, 10) {
		return errors.New("credit card numer is not valid")
	}
	return nil
}

func isEven(num int) bool {
	return num%2 == 0
}

func isDivisible(num int, divider int) bool {
	return num%divider == 0
}

func reverseString(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
