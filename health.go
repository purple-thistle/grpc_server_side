package main

import (
	"context"

	pb "gitlab.com/purple-thistle/grpc_server_side_api"
)

type HealthServer struct {
	pb.UnsafeHealthServer
}

func (h *HealthServer) HealthCheck(ctx context.Context, input *pb.HealthRequest) (*pb.HealthResponse, error) {
	return &pb.HealthResponse{Alive: true}, nil
}
