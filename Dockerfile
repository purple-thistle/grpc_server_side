FROM debian:bullseye-slim

RUN apt-get update

RUN apt-get install ca-certificates -y

ADD server /usr/local/bin/

RUN chmod 755 /usr/local/bin/server

ENTRYPOINT ["/bin/bash", "-c"]

CMD ["/usr/local/bin/server"]