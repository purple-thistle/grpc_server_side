package main

import (
	"testing"
)

func Test_validate(t *testing.T) {
	type args struct {
		cc string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "valid",
			args:    args{cc: "345544607390383"},
			wantErr: false,
		},
		{
			name:    "invalid",
			args:    args{cc: "3455446073903893"},
			wantErr: true,
		},
		{
			name:    "invalid",
			args:    args{cc: "345544607390389a3"},
			wantErr: true,
		},
		{
			name:    "short",
			args:    args{cc: "3"},
			wantErr: true,
		},
		{
			name:    "empty",
			args:    args{cc: ""},
			wantErr: true,
		},
		{
			name:    "with spaces",
			args:    args{cc: "4539 3195 0343 6467"},
			wantErr: false,
		},
		{
			name:    "with hyphens",
			args:    args{cc: "6771-8983-8150-5750"},
			wantErr: false,
		},
		{
			name:    "invalid with spaces",
			args:    args{cc: "4239 3195 0343 6469"},
			wantErr: true,
		},
		{
			name:    "invalid with hyphens",
			args:    args{cc: "6771-8883-8250-5751"},
			wantErr: true,
		},
		{
			name:    "not digits",
			args:    args{cc: "qwerty"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validate(tt.args.cc); (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_reverseString(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "letters",
			args: args{s: "abc"},
			want: "cba",
		},
		{
			name: "numbers",
			args: args{s: "1234"},
			want: "4321",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := reverseString(tt.args.s); got != tt.want {
				t.Errorf("reverseString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_decode(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "valid",
			args:    args{message: ".... . .-.. .-.. --- --..--/.-- --- .-. .-.. -.."},
			want:    "HELLO, WORLD",
			wantErr: false,
		},
		{
			name:    "with letters",
			args:    args{message: ".... ag. .-.. .-.. --- --..--/.-- --- .-. .-.. -.."},
			want:    "",
			wantErr: true,
		},
		{
			name:    "with symbols",
			args:    args{message: ".... . .-.. .-.. --- --..--?/.-- --- .-. ! .-.. -.."},
			want:    "",
			wantErr: true,
		},
		{
			name:    "valid",
			args:    args{message: "- .... .. .../.. .../-. --- -/.-/.--. .. .--. ."},
			want:    "THIS IS NOT A PIPE",
			wantErr: false,
		},
		{
			name:    "empty",
			args:    args{message: ""},
			want:    "",
			wantErr: false,
		},
		{
			name:    "non existent character",
			args:    args{message: "------"},
			want:    "",
			wantErr: true,
		},
		// {
		// 	name:    "space",
		// 	args:    args{message: " "},
		// 	want:    "",
		// 	wantErr: false,
		// },
		// {
		// 	name:    "space and slash",
		// 	args:    args{message: " / "},
		// 	want:    "",
		// 	wantErr: false,
		// },
		// {
		// 	name:    "slash",
		// 	args:    args{message: "/"},
		// 	want:    "",
		// 	wantErr: false,
		// },
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := decode(tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("decode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("decode() = %v, want %v", got, tt.want)
			}
		})
	}
}
